package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.AreaCientificaDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;;

public class AreaCientificaBC extends PatternCrudBC<AreaCientifica, AreaCientificaDTO> {

    private static AreaCientificaBC instance = new AreaCientificaBC();

    private AreaCientificaBC() {

    }

    @Override
    protected void validateForDataModification(AreaCientifica object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0010");
        }
        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }
        if (object.getNome() == null) {
            throw new SeminariosCientificosException("ER0010");
        }

    }

    @Override
    protected boolean validateForFindData(AreaCientificaDTO object) {
        return object != null &&
            (!StringUtils.isBlank(object.getNome())
                || !StringUtils.isBlank(object.getNome())
                || object.getId() != null);
    }

    public static AreaCientificaBC getInstance() {
        return instance;
    }

    public static void setInstance(AreaCientificaBC instance) {
        AreaCientificaBC.instance = instance;
    }

}
