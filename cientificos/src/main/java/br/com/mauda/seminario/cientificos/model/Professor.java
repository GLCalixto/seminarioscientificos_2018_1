package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements IdentifierInterface, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3423053687365978254L;
    private Long id;
    private String email;
    private String nome;
    private String telefone;
    private Double salario;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

    public Professor(Instituicao inst) {
        this.instituicao = inst;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;

    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public void setSeminario(List<Seminario> seminario) {
        this.seminarios = seminario;
    }

}
