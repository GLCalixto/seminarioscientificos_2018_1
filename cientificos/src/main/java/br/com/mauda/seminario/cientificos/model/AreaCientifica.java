package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AreaCientifica implements IdentifierInterface, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6759735305032264386L;

    private Long id;
    private String nome;
    private List<Curso> cursos = new ArrayList<>();
    private List<Seminario> seminarios = new ArrayList<>();

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;

    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public void setSeminarios(List<Seminario> seminarios) {
        this.seminarios = seminarios;
    }

}
