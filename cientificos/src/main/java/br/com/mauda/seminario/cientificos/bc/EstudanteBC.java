package br.com.mauda.seminario.cientificos.bc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.EstudanteDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;

public class EstudanteBC extends PatternCrudBC<Estudante, EstudanteDTO> {

    private static EstudanteBC instance = new EstudanteBC();

    private EstudanteBC() {
    }

    @Override
    protected void validateForDataModification(Estudante object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0031");
        }
        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0031");
        }
        if (StringUtils.isBlank(object.getEmail())) {
            throw new SeminariosCientificosException("ER0030");
        }
        if (object.getEmail().length() > 50) {
            throw new SeminariosCientificosException("ER0030");
        }
        if (StringUtils.isBlank(object.getTelefone())) {
            throw new SeminariosCientificosException("ER0032");
        }
        if (object.getTelefone().length() > 15) {
            throw new SeminariosCientificosException("ER0032");
        }
        Pattern p = Pattern.compile(
            "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        Matcher m = p.matcher(object.getEmail());
        boolean matchFound = m.matches();

        if (!matchFound) {
            throw new SeminariosCientificosException("ER0030");
        }
        InstituicaoBC.getInstance().validateForDataModification(object.getInstituicao());

    }

    @Override
    protected boolean validateForFindData(EstudanteDTO object) {

        return object != null &&
            (object.getId() != null
                || !StringUtils.isBlank(object.getNome())
                || !StringUtils.isBlank(object.getCidade())
                || !StringUtils.isBlank(object.getEmail())
                || !StringUtils.isBlank(object.getEstado())
                || !StringUtils.isBlank(object.getTelefone())
                || !StringUtils.isBlank(object.getNomeInstituicao())
                || !StringUtils.isBlank(object.getPais()));
    }

    public static EstudanteBC getInstance() {
        return instance;
    }

    public static void setInstance(EstudanteBC instance) {
        EstudanteBC.instance = instance;
    }

}
