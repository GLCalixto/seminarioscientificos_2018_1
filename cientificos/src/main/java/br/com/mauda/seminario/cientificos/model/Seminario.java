package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario implements IdentifierInterface, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8978260264522685251L;

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private List<Professor> professores = new ArrayList<>();
    private Date data;
    private Integer qtdinscricoes;
    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario(AreaCientifica area, Professor professor, Integer qtdinscricoes) {
        this.areasCientificas.add(area);
        this.professores.add(professor);
        this.qtdinscricoes = qtdinscricoes;
        professor.getSeminarios().add(this);
        for (int i = 0; i < qtdinscricoes; i++) {
            this.getInscricoes().add(new Inscricao(this));
        }

    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;

    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdinscricoes;
    }

    public void setQtdInscricoes(Integer qtdinscricoes) {
        this.qtdinscricoes = qtdinscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public Integer getQtdinscricoes() {
        return this.qtdinscricoes;
    }

    public void setQtdinscricoes(Integer qtdinscricoes) {
        this.qtdinscricoes = qtdinscricoes;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }

}
