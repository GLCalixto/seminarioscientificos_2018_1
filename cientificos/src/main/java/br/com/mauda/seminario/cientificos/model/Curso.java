package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

public class Curso implements IdentifierInterface, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -872997917213735435L;

    private Long id;
    private String nome;
    private AreaCientifica area;

    public Curso(AreaCientifica area) {
        this.area = area;
        this.area.getCursos().add(this);
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;

    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.area;
    }

    public void setAreaCientifica(AreaCientifica area) {
        this.area = area;
    }

}
