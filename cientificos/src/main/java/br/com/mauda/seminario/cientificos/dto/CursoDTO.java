package br.com.mauda.seminario.cientificos.dto;

import br.com.mauda.seminario.cientificos.model.IdentifierInterface;

public class CursoDTO implements IdentifierInterface {

    private Long id;
    private Long idAreaCientifica;
    private String nome;
    private String nomeAreaCientifica;

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;
    }

    public Long getIdAreaCientifica() {
        return this.idAreaCientifica;
    }

    public void setIdAreaCientifica(Long idAreaCientifica) {
        this.idAreaCientifica = idAreaCientifica;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeAreaCientifica() {
        return this.nomeAreaCientifica;
    }

    public void setNomeAreaCientifica(String nomeAreaCientifica) {
        this.nomeAreaCientifica = nomeAreaCientifica;
    }

}
