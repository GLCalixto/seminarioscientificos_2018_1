package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.InstituicaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Instituicao;

public class InstituicaoBC extends PatternCrudBC<Instituicao, InstituicaoDTO> {

    private static InstituicaoBC instance = new InstituicaoBC();

    @Override
    protected void validateForDataModification(Instituicao object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (StringUtils.isBlank(object.getCidade())) {
            throw new SeminariosCientificosException("ER0050");
        }
        if (object.getCidade().length() > 50) {
            throw new SeminariosCientificosException("ER0050");
        }
        if (StringUtils.isBlank(object.getEstado())) {
            throw new SeminariosCientificosException("ER0051");
        }
        if (object.getEstado().length() > 50) {
            throw new SeminariosCientificosException("ER0051");
        }
        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0052");
        }
        if (object.getNome().length() > 100) {
            throw new SeminariosCientificosException("ER0052");
        }
        if (StringUtils.isBlank(object.getPais())) {
            throw new SeminariosCientificosException("ER0053");
        }
        if (object.getPais().length() > 50) {
            throw new SeminariosCientificosException("ER0053");
        }
        if (StringUtils.isBlank(object.getSigla())) {
            throw new SeminariosCientificosException("ER0054");
        }
        if (object.getSigla().length() > 10) {
            throw new SeminariosCientificosException("ER0054");
        }

    }

    @Override
    protected boolean validateForFindData(InstituicaoDTO object) {

        return object != null
            && (object.getId() != null
                || !StringUtils.isBlank(object.getCidade())
                || !StringUtils.isBlank(object.getEstado())
                || !StringUtils.isBlank(object.getNome())
                || !StringUtils.isBlank(object.getPais())
                || !StringUtils.isBlank(object.getSigla()));
    }

    public static InstituicaoBC getInstance() {
        return instance;
    }

}
