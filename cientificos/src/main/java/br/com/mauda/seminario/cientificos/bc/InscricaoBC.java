package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.InscricaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoBC extends PatternCrudBC<Inscricao, InscricaoDTO> {

    private static InscricaoBC instance = new InscricaoBC();

    private InscricaoBC() {
    }

    @Override
    protected void validateForDataModification(Inscricao object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (object.getSituacao() == null) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (object.getSituacao() != SituacaoInscricaoEnum.DISPONIVEL) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (object.getDireitoMaterial() == null) {
            throw new SeminariosCientificosException("ER0041");
        }
        EstudanteBC.getInstance().validateForDataModification(object.getEstudante());
        SeminarioBC.getInstance().validateForDataModification(object.getSeminario());
    }

    @Override
    protected boolean validateForFindData(InscricaoDTO object) {
        return object != null &&
            (object.getId() != null
                || !StringUtils.isBlank(object.getTitulo())
                || !StringUtils.isBlank(object.getNomeEstudante())
                || object.getDireitoMaterial() != null
                || object.getData() != null
                || object.getSituacoes() != null);
    }

    public void comprar(Inscricao inscricao, Estudante estudante, Boolean direitoMaterial) {
        if (inscricao == null) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (inscricao.getSituacao() != SituacaoInscricaoEnum.DISPONIVEL) {
            throw new SeminariosCientificosException("ER0042");
        }
        if (estudante == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (direitoMaterial == null) {
            throw new SeminariosCientificosException("ER0041");
        }
        EstudanteBC.getInstance().validateForDataModification(estudante);
        inscricao.comprar(estudante, direitoMaterial);
    }

    public void realizarCheckIn(Inscricao inscricao) {
        if (inscricao == null) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (inscricao.getSituacao() != SituacaoInscricaoEnum.COMPRADO) {
            throw new SeminariosCientificosException("ER0043");
        }
        inscricao.realizarCheckIn();
    }

    public static InscricaoBC getInstance() {
        return instance;
    }

}
