package br.com.mauda.seminario.cientificos.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.mauda.seminario.cientificos.model.IdentifierInterface;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoDTO implements IdentifierInterface {

    private Long id;
    private Date data;
    private Boolean direitoMaterial;
    private String nomeEstudante;
    private List<SituacaoInscricaoEnum> situacoes = new ArrayList<>();
    private String titulo;

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public String getNomeEstudante() {
        return this.nomeEstudante;
    }

    public void setNomeEstudante(String nomeEstudante) {
        this.nomeEstudante = nomeEstudante;
    }

    public List<SituacaoInscricaoEnum> getSituacoes() {
        return this.situacoes;
    }

    public void setSituacoes(List<SituacaoInscricaoEnum> situacoes) {
        this.situacoes = situacoes;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}
