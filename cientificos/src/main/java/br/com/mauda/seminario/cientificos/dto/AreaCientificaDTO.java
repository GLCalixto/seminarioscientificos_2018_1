package br.com.mauda.seminario.cientificos.dto;

import br.com.mauda.seminario.cientificos.model.IdentifierInterface;

public class AreaCientificaDTO implements IdentifierInterface {

    private Long id;
    private String nome;

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.id;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
