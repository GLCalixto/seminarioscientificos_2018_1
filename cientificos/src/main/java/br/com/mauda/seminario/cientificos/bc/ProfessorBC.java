package br.com.mauda.seminario.cientificos.bc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.ProfessorDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Professor;

public class ProfessorBC extends PatternCrudBC<Professor, ProfessorDTO> {

    private static ProfessorBC instance = new ProfessorBC();

    private ProfessorBC() {
    }

    @Override
    protected void validateForDataModification(Professor object) {

        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0061");
        }
        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0061");
        }
        if (StringUtils.isBlank(object.getEmail())) {
            throw new SeminariosCientificosException("ER0060");
        }
        if (object.getEmail().length() > 50) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (StringUtils.isBlank(object.getTelefone())) {
            throw new SeminariosCientificosException("ER0062");
        }
        if (object.getTelefone().length() > 15) {
            throw new SeminariosCientificosException("ER0062");
        }
        if (object.getSalario() == null) {
            throw new SeminariosCientificosException("ER0063");
        }
        if (object.getSalario() < 0) {
            throw new SeminariosCientificosException("ER0063");
        }
        if (object.getSalario() == 0) {
            throw new SeminariosCientificosException("ER0063");
        }
        Pattern p = Pattern.compile(
            "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        Matcher m = p.matcher(object.getEmail());
        boolean matchFound = m.matches();

        if (!matchFound) {
            throw new SeminariosCientificosException("ER0060");
        }

        InstituicaoBC.getInstance().validateForDataModification(object.getInstituicao());
    }

    @Override
    protected boolean validateForFindData(ProfessorDTO object) {

        return object != null &&
            (object.getId() != null
                || !StringUtils.isBlank(object.getCidade())
                || !StringUtils.isBlank(object.getEstado())
                || !StringUtils.isBlank(object.getEmail())
                || !StringUtils.isBlank(object.getNomeInstituicao())
                || !StringUtils.isBlank(object.getNome())
                || !StringUtils.isBlank(object.getTelefone())
                || !StringUtils.isBlank(object.getPais()));
    }

    public static ProfessorBC getInstance() {
        return instance;
    }

    public static void setInstance(ProfessorBC instance) {
        ProfessorBC.instance = instance;
    }

}
