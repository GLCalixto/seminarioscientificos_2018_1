package br.com.mauda.seminario.cientificos.bc;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.SeminarioDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Seminario;

public class SeminarioBC extends PatternCrudBC<Seminario, SeminarioDTO> {

    private static SeminarioBC instance = new SeminarioBC();

    @Override
    protected void validateForDataModification(Seminario object) {

        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (object.getData() == null) {
            throw new SeminariosCientificosException("ER0070");
        }
        if (object.getDescricao() == null) {
            throw new SeminariosCientificosException("ER0071");
        }
        if (object.getTitulo() == null) {
            throw new SeminariosCientificosException("ER0072");
        }
        if (object.getTitulo().length() > 50) {
            throw new SeminariosCientificosException("ER0072");
        }
        if (object.getTitulo() == "     ") {
            throw new SeminariosCientificosException("ER0072");
        }
        if (object.getMesaRedonda() == null) {
            throw new SeminariosCientificosException("ER0073");
        }
        if (object.getDescricao().length() > 200) {
            throw new SeminariosCientificosException("ER0071");
        }
        if (object.getDescricao() == "     ") {
            throw new SeminariosCientificosException("ER0071");
        }
        if (object.getQtdinscricoes() == null) {
            throw new SeminariosCientificosException("ER0074");
        }
        if (object.getQtdinscricoes() <= -1) {
            throw new SeminariosCientificosException("ER0074");
        }
        if (object.getQtdinscricoes() == 0) {
            throw new SeminariosCientificosException("ER0074");
        }
        if (object.getData().before(new Date())) {
            throw new SeminariosCientificosException("ER0070");
        }
        if (object.getProfessores().contains(null)) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (object.getProfessores().isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }
        if (object.getAreasCientificas().isEmpty()) {
            throw new SeminariosCientificosException("ER0076");
        }
        if (object.getAreasCientificas().contains(null)) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (object.getAreasCientificas().size() > 50) {
            throw new SeminariosCientificosException("ER0076");
        }
        // varivel recebendo o ultimo valor do array de objetos para validação, sendo validado no metodo da propria classe
        int a = object.getAreasCientificas().size();
        a--;
        AreaCientificaBC.getInstance().validateForDataModification(object.getAreasCientificas().get(a));
        // leia o comentario anterior
        int b = object.getProfessores().size();
        b--;
        ProfessorBC.getInstance().validateForDataModification(object.getProfessores().get(b));
    }

    @Override
    protected boolean validateForFindData(SeminarioDTO object) {
        // TODO Auto-generated method stub
        return object != null &&

            (!StringUtils.isBlank(object.getDescricao())
                || !StringUtils.isBlank(object.getNomeAreaCientifica())
                || !StringUtils.isBlank(object.getNomeProfessor())
                || !StringUtils.isBlank(object.getTitulo())
                || object.getData() != null
                || object.getMesaRedonda() != null
                || object.getId() != null);

    }

    public static SeminarioBC getInstance() {
        return instance;
    }

    public static void setInstance(SeminarioBC instance) {
        SeminarioBC.instance = instance;
    }

}
