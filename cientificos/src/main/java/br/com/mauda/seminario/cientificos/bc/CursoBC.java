package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dto.CursoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Curso;

public class CursoBC extends PatternCrudBC<Curso, CursoDTO> {

    private static CursoBC instance = new CursoBC();

    private CursoBC() {
    }

    @Override
    protected void validateForDataModification(Curso object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0020");
        }
        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0020");
        }

        if (object.getAreaCientifica() == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (object.getAreaCientifica().getNome() == null) {
            throw new SeminariosCientificosException("ER0010");
        }
        if (object.getAreaCientifica().getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }
        if (object.getAreaCientifica().getNome() == "     ") {
            throw new SeminariosCientificosException("ER0010");
        }

        // AreaCientificaBC.getInstace().validateForDataModification(object.getAreaCientifica());

    }

    @Override
    protected boolean validateForFindData(CursoDTO object) {

        return object != null &&
            (!StringUtils.isBlank(object.getNome())
                || !StringUtils.isBlank(object.getNomeAreaCientifica())
                || object.getId() != null
                || object.getIdAreaCientifica() != null);
    }

    public static CursoBC getInstance() {
        return instance;
    }

    public static void setInstance(CursoBC instance) {
        CursoBC.instance = instance;
    }

}
